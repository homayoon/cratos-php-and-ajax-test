<?php
require __DIR__ . '/app/db.php';

// Create database
try{
    $conn = createPDOConnection($CONFIG);
    $sql = "CREATE DATABASE {$CONFIG['dbName']}";
    $conn->query($sql);
    $conn = null;
}catch (Exception $e){
    echo $e->getMessage();
    exit();
}
//Create table
try {
    $conn = createPDOConnection($CONFIG, true);
    $sql = "CREATE TABLE users (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        first_name VARCHAR(30) NOT NULL,
        last_name VARCHAR(30) NOT NULL,
        image VARCHAR(255),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";

    $conn->exec($sql);
} catch(PDOException $e) {
   echo $e->getMessage();
   exit();
}
echo 'Database And Tables Created Successfully!';
