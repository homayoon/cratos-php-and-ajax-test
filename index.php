<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cratos PHP AJAX Test</title>
  </head>
  <body>
  <h1>Cratos Ajax Form Test</h1>

  <form action="app/controller.php" method="post" enctype="multipart/form-data" id="create_form">
    <label for="firstName">First Name:</label>
    <input type="text" id="firstName" name="firstName" placeholder="First Name">
    <label for="lastName">Last Name:</label>
    <input type="text" id="lastName" name="lastName" placeholder="Last Name">
    <label for="image">Image:</label>
    <input type="file" id="image" name="image" placeholder="Select Image">
    <button type="button" id="btn_submit">Submit</button>
  </form>
  <p style="color: red" id="error_text"></p>
  <hr>

  <table>
    <thead>
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Image</th>
    </tr>
    </thead>

    <tbody id="table_body">
    <!--  Handle Show Items From Db  -->
    <?php
      require 'app/db.php';
      $conn = createPDOConnection($CONFIG, true);
      $stmt = $conn->prepare("SELECT id, first_name, last_name, image FROM users");
      $stmt->execute();
      $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
      foreach($stmt->fetchAll() as $row) {
        echo "<tr>
               <td>{$row['first_name']}</td>
               <td>{$row['last_name']}</td>
               <td><img src='{$row['image']}' width='40' height='auto'></td>
             </tr>";
      }
    ?>


    </tbody>
  </table>

    <script>

        // Handle Store And Show Item
        const handleSubmitForm = (event) => {
            event.preventDefault();
            let formElement = document.getElementById('create_form');
            let formData = new FormData(formElement)
            let errorTextElem = document.getElementById('error_text');
            errorTextElem.innerText = '';
            fetch('app/ajax-insert.php', {
              method: 'POST',
              body: formData
            })
            .then(response => response.json())
            .then(data => {
                let {message, success} = data;
                if(!success && message.error_message){
                    errorTextElem.innerText = message.error_message
                    return;
                }
                let elem =  '<td>'+message.first_name+'</td>' +
                            '<td>'+message.last_name+'</td>' +
                            '<td><img src="'+message.image+'" width="70" height="auto"></td>'

                let row = document.createElement('tr');
                row.innerHTML = elem

                document.getElementById('table_body').append(row)

                formElement.reset();
            })
            .catch((error) => {
                console.log('Error:', error);
            });
        }

        // Event Handler For Form Submit
        document.getElementById("btn_submit").addEventListener('click',handleSubmitForm);
    </script>
  </body>
</html>
