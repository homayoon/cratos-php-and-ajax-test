<?php

// Handle Json Response
function json_response($code = 200, $message = null)
{
    // clear the old headers
    header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
    // treat this as json
    header('Content-Type: application/json');
    $status = array(
        200 => '200 OK',
        400 => '400 Bad Request',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
    );
    // ok, validation error, or failure
    header('Status: '.$status[$code]);

    return json_encode(array(
        'status' => $code < 300,
        'message' => $message
    ));
}

// Image Uploader
function uploadImage($file)
{
    $type = explode('/',$file['type'])[1];

    if($type != "jpg" && $type != "png" && $type != "jpeg") return false;

    $upFile = '../assets/img/'.$file["name"];
    if(is_uploaded_file($file["tmp_name"])) {
        move_uploaded_file($file["tmp_name"], $upFile);
    }

    return 'assets/img/'.$file["name"];
}
