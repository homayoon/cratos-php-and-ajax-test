<?php

require 'functions.php';
$CONFIG = include(__DIR__.'/../config.php');

// Create PDO Connection
function createPDOConnection($config, $withDB = false){
    $dbName = $withDB ? 'dbname='.$config['dbName'] : '';
    $conn = new PDO("mysql:host={$config['dbHost']};$dbName", $config['dbUsername'], $config['dbPassword']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
}
