<?php

require 'db.php';

// if you are doing ajax with application-json headers
if (empty($_POST)) {
    $_POST = json_decode(file_get_contents("php://input"), true) ? : [];
}

$conn = createPDOConnection($CONFIG, true);

$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$image = '';
if($_FILES["image"]){
    $image = uploadImage($_FILES['image']);
    if (!$image){
        echo json_response(400, [
            'error_message' => 'Sorry, only JPG, JPEG, PNG files are allowed.'
        ]);
        exit();
    }
}
$sql = "INSERT INTO `users` (`first_name`, `last_name`, `image`) VALUES ('{$firstName}', '{$lastName}', '{$image}')";
try {
    $conn->exec($sql);
    echo json_response(200, [
        'first_name' => $firstName,
        'last_name' => $lastName,
        'image' => $image
    ]);
}catch (Exception $e){
    echo json_response(400, $e->getMessage());
}
